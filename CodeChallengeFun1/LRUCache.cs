﻿using System;
using System.Collections.Generic;

namespace CodeChallengeFun1
{
    public class LRUCache
    {
        private HashSet<int> _pages;
        private LinkedList<int> _lruCache;
        private int _cacheLimit;
        public LRUCache(int limit)
        {
            _pages = new HashSet<int>(); // key: page, value: address in linkedlist
            _lruCache = new LinkedList<int>(); // 
            _cacheLimit = limit;
        }

        // Set limit to linkedList 
        // When you reach limit, eject the last item in the queue
        // if there is a hit, place that item in the beginning of the list

        public string AddItem(int page)
        {
            // when we haven't hit the limit, and it isn't a HIT. add the item to cache with no issue
            if(_lruCache.Count != _cacheLimit && !_pages.Contains(page) )
            {
                _lruCache.AddFirst(page);
                _pages.Add(page);
                return "insert";
            }

            // if we make a cache hit, doesn't matter if we haven't reached the limit yet
            if (_pages.Contains(page))
            {
                // Swap the location where it exists and place in front of lru cache
                _lruCache.Remove(page);
                _lruCache.AddFirst(page);
                return "hit";
            }

            // if we hit a limit and there's no hit, lets evict one and add
            //if(_lruCache.Count == _cacheLimit)
            //{
            //    _lruCache.RemoveLast();
            //    _lruCache.AddFirst(page);
            //    return "evict"; 
            //}
            
            _lruCache.RemoveLast();
            _lruCache.AddFirst(page);
            return "evict";
            

        }


    }
}
