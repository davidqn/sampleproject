﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace CodeChallengeFun1
{
    public class CallbackProject
    {
        public bool Main()
        {
            
            var result = SearchStuff<bool>((nameList) => {
                
                foreach (var name in nameList)
                {
                    var newName = $"{name} hehe";
                }
                return true;
            });

            return result;
        }
        public double Diff(double x, Func<double, double> f)
        {
            double h = 0.0000001;
            
            return (f(x + h) - f(x)) / h;
        }

        // public delegate TResult Func<in T, out TResult>(T arg);
        // the first argument is the input and the second is the output
        public bool SearchStuff<T>(Func<List<string>, T> callback)
        {
            var names = new List<string>() { "david", "jim" };

            callback(names);

            return true;
        }
    }
}
