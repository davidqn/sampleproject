using CodeChallengeFun1;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            // REF: https://www.geeksforgeeks.org/lru-cache-implementation/

            var lruCache = new LRUCache(limit: 3);
            string message;

            message = lruCache.AddItem(3);
            message = lruCache.AddItem(2);
            message = lruCache.AddItem(5);

            // Cache hit
            message = lruCache.AddItem(3);
            Assert.Equal("hit", message);

            // Cache hit
            lruCache.AddItem(2);
            Assert.Equal("hit", message);

            // Cache evict
            message = lruCache.AddItem(9);
            Assert.Equal("evict", message);
            message = lruCache.AddItem(10);
            Assert.Equal("evict", message);
            message = lruCache.AddItem(11);
            Assert.Equal("evict", message);

            // Seems to work perfectly fine
        }

        [Fact]
        public void Test2()
        {
            var callbackProject = new CallbackProject();
            var result = callbackProject.Main();

            Assert.Equal(true, result);
        }
    }
}
