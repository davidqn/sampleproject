﻿using System;
using Xunit;
using CodeChallengeFun1;

namespace CodeChallengeTest
{
    
    public class LRUCacheTest
    {

        [Fact]
        public void TestLRUCache()
        {
            var lruCache = new LRUCache();
            string message;

            message = lruCache.AddItem(3);
            message = lruCache.AddItem(2);
            message = lruCache.AddItem(5);

            // Cache hit
            message = lruCache.AddItem(3);
            Assert.Equal("hit", message);

            // Cache hit
            lruCache.AddItem(2);
            Assert.Equal("hit", message);

            // Cache evict
            message = lruCache.AddItem(9);
            Assert.Equal("evict", message);
            message = lruCache.AddItem(10);
            Assert.Equal("evict", message);
            message = lruCache.AddItem(11);
            Assert.Equal("evict", message);

        }
    }
}
